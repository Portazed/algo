__license__ = 'GolluM (c) EPITA'
__docformat__ = 'reStructuredText'
__revision__ = '$Id: huffman.py 2016-04-04'

from binaryTrees import * #LLLLLLLLLRQI?GIRQFVT?JTVEJQ

################################################################################
## COMPRESSION

def buildFrequencyList(outputList, dataIN):
    """Build a tuple list according to the character frequencies in the input.

        :param outputList: the list to store the result. Each value is a tuple which first element is the frequency of the character stored in the second part.
        :param dataIN: the data from which we want to build the frequencies list.
        :type outputList: tuple<int, str> list
        :type dataIN: str

        :Example:

        >>> L = []
        >>> buildFrequencyList(L, 'bbaabtttaabtctce')
        >>> L
        [(4, 'b'), (4, 'a'), (5, 't'), (2, 'c'), (1, 'e')]
    """
    # FIXME
    for c in dataIN:
        i = 0
        b = False
        entier = 0
        char = ''
        while i < len(outputList) and not b :
            (entier, char) = outputList[i]
            b = c == char
            if not b:                
                i+=1
                        
            
        if i < len(outputList) and b:
                entier += 1
                outputList[i] = (entier,char)
        else:
            outputList.append((1, c))

#L = []
#buildFrequencyList(L, 'bbaabtttaabtctce')
#print(L)

def searchMin(L):
    (val,elt) = (5000,'')
    k = 0
    for i in range(0, len(L)):
        (val1,elt1) = L[i].key
        if val >= val1:
            (val, elt) = L[i].key
            k = i
    B = L[k]
    for j in range(k, len(L)-1):
        L[j] = L[j+1]
    L.pop()
    return B    

def cleanTree(B):
    if B.left == None and B.right == None:
        (a,b) = B.key
        B.key = b
    else:
        B.key = ''
        if B.left != None:
            cleanTree(B.left)
        if B.right != None:
            cleanTree(B.right)

def buildHuffmanTree(inputList):
    """Process the frequency list into an Huffman tree according to the algorithm.

        :param inputList: the frequencies list from :func:`buildFrequencyList`.
        :type inputList: tuple<int, str> list
        :return: returns an huffman tree containing all the datas from the list.
        :rtype: BinTree

        :Example:

        >>> H = buildHuffmanTree(L)
        >>> prettyPrint(H)
               .
              / \\
             /   \\
            /     \\
           /       \\
           .       .
          / \     / \\
         /   \   /   \\
         t   b   a   .
                    / \\
                    c e
    """
    # FIXME
    l = []
    for i in range(len(inputList)):
        (entier,char) = inputList[i]
        B = BinTree()
        B.key = (entier,char)
        B.left = None
        B.right = None
        #print(entier,' ',char)
        l.append(B)               
    
    D = searchMin(l)
    G = searchMin(l)
    (d1,d2) = D.key
    (g1,g2) = G.key
    B = newBinTree(d1+g1,G,D)
    A = newBinTree(1,None,None)
    while len(l) > 0:
        if len(l)%2 == 1:        
            G = searchMin(l)
            D = B
            (g1,g2) = G.key
            B = newBinTree(D.key+g1,G,D)
        else:
            D = searchMin(l)
            G = searchMin(l) 
            (d1,d2) = D.key
            (g1,g2) = G.key
            A = newBinTree(d1+g1,G,D)
            G = A
            D = B
            B = newBinTree(D.key+G.key,G,D)
    cleanTree(B)
    return B

def encodeDataRec(huffmanTree, s='', l=[]): #Construit la liste contenant les feuilles et leur chemin
    if huffmanTree.left == None and huffmanTree.right == None:
        l.append((huffmanTree.key, s))
    if huffmanTree.left != None:
        encodeDataRec(huffmanTree.left, s+'0',l)
    if huffmanTree.right != None:
        encodeDataRec(huffmanTree.right, s+'1',l)
    
def encodeData(huffmanTree, dataIN):
    """Encode the input string to its binary string representation.

        :param huffmanTree: the huffman tree from :func:`buildHuffmanTree`.
        :param dataIN: the data we want to encode.
        :type huffmanTree: BinTree
        :type dataIN: str
        :return: returns the binary string.
        :rtype: str

        :Example:

        >>> encodeData(H, 'bbaabtttaabtctce')
        '01011010010000001010010011000110111'
         0101101000000100100101011000111
    """
    # FIXME
    s = ''
    l = []
    if huffman != None:
        encodeDataRec(huffmanTree, '', l)
        t = len(l)
        for c in dataIN:
            i = 0
            b = False
            s2 = ''
            while i < t and not b:
                (elt,s2) = l[i]
                b = c == elt
                i += 1
            s += s2
    return s

huffman = newBinTree('.',newBinTree('.', newBinTree('t',None,None), newBinTree('b',None,None)), 
                     newBinTree('.',newBinTree('a',None,None), newBinTree('.',newBinTree('c',None,None),
                     newBinTree('e',None,None))))
#print(encodeData(huffman,'bbaabtttaabtctce'))
def encodeTree(huffmanTree):
    """Encodes an huffman tree to its binary representation using a preOrder traversal:
        * each leaf key is encoded into its binary representation on 8 bits preceded by '1'
        * each time we go left we add a '0' to the result

        :param huffmanTree: the huffman tree to encode.
        :type huffmanTree: BinTree
        :return: returns a string corresponding to the binary representation of the huffman tree.
        :rtype: str

        :Example:

        >>> encodeTree(H)
        '0010111010010110001001011000010101100011101100101'
    """
    # FIXME
    if huffmanTree.left == None and huffmanTree.right == None:
        val = ord(huffmanTree.key)
        s = ''
        while val > 0:
            s = str(val%2) + s
            val = val // 2
        while len(s) < 8:
            s = '0' + s
        return '1' + s
    else:        
        if huffmanTree.left != None and huffmanTree.right != None:
            return '0' + encodeTree(huffmanTree.left) + encodeTree(huffmanTree.right)
        elif huffmanTree.right != None and huffmanTree.left == None:                
            return encodeTree(huffmanTree.right)
        elif huffmanTree.left != None and huffmanTree.right == None:
            return '0' + encodeTree(huffmanTree.left)
            
#print(encodeTree(huffman))   


def toBinary(dataIN):
    """Compress a string containing the binary representation to its real compressed string.

        :param dataIN: the data to compress.
        :type dataIN: str
        :return: returns a tuple: the compressed string corresponding to the input and the number of bits for the alignment.
        :rtype: tuple<str, int>

        :Example:
                      101010000100110001000000101111
        >>> toBinary('01011010010000001010010011000110111')
        ('Z@¤Æ\\x07', 5)
        >>> toBinary('0010111010010110001001011000010101100011101100101')
        ('.\\x96%\\x85c²\\x01', 7)

        .. warning:: some characters in the output string may not be visibles if you print it.
    """
    t=len(dataIN)
    res=''
    align = 0
    m = 0
    for j in range(t//8):
        s=''
        for i in range(m,m+8):
            s+=dataIN[i]
        res+=chr(toInt(s))    
            
        m += 8
    align=8-(t-m)
    k=''
    for i in range(m,t):
        k+=dataIN[i]

    res+=chr(toInt(k)) 
    return (res,align)

def toInt(s):#convertit une chaîne de caractère(binaire) en entier
   res=0
   
   for i in range(len(s)):
       res = res * 2 + ord(s[i]) - 48
   return res


#print(toBinary('01011010010000001010010011000110111'))
def compression(dataIN):
    """Compress a string using the Huffman algorithm.

        :param dataIN: the data to compress.
        :type dataIN: str
        :return: returns the compressed data (and its number of bits for the alignment) and the compressed tree (and its number of bits for the alignment).
        :rtype: tuple< tuple<str, int>, tuple<str, int> >

        :Example:

        >>> compression('bbaabtttaabtctce')
        (('Z@¤Æ\\x07', 5), ('.\\x96%\\x85c²\\x01', 7))
    """
    # FIXME
    L = []
    buildFrequencyList(L,dataIN)
    B = buildHuffmanTree(L)
    return (toBinary(encodeData(B,dataIN)),toBinary(encodeTree(B)))
    


################################################################################
## DECOMPRESSION


def decodeTree(dataIN, alignement):
    """Decodes an huffman tree from it binary representation:
        * a '0' means we add a new internal node and go to its left node
        * an '1' means the next 8 values are the encoded character of the current leaf.

        :param dataIN: the real binary string containing the encoded huffman tree.
        :param alignement: the number of bits to ignore at the end of the input.
        :type dataIN: str
        :type alignement: int
        :return: returns decoded huffman tree
        :rtype: BinTree

        :Example:

        >>> H = decodeTree('.\\x96%\\x85c²\\x01', 7)
        >>> prettyPrint(H)
               .
              / \\
             /   \\
            /     \\
           /       \\
           .       .
          / \     / \\
         /   \   /   \\
         t   b   a   .
                    / \\
                    c e
    """
    s = ''
    for c in dataIN:
        v = ord(c)
        inter = ''
        while v > 0:
            inter = str(v%2) + inter   
            v = v //2
        if len(inter) < 8 and c != dataIN[len(dataIN)-1]:
            inter = '0' + inter
        s += inter
    print(s) 
    B = newBinTree('',None,None)
    A = B
    i = 0
    while i < len(s)-alignement:
        #printTree(B)
        if s[i] == '0':
            C = newBinTree('',None,None)
            B.left = C
            B = C
            i += 1
        else:
            i += 1
            k = 0
            val = 0
            while i < len(s) - alignement and i < 8:
                print(s[i])
                val += (2*int(s[i]))**(7-k)
                i += 1
            #print(val)
            C = newBinTree(chr(val),None,None)
            if B.left == None:
                B.left = C
            elif B.right == None:
                B.right = C   
    return A

L = []
buildFrequencyList(L,'bbaabtttaabtctce')
B = buildHuffmanTree(L)
(a,b) = toBinary(encodeData(B,'bbaabtttaabtctce'))
B = decodeTree('.\\x96%\\x85c²\\x01', 7)
#printTree(B)

def decodeData(huffmanTree, dataIN, alignement):
    """Decode a binary string using the corresponding huffman tree into something more readable.

        :param huffmanTree: the huffman tree for decoding.
        :param dataIN: the input binary string we want to decode.
        :param alignement: the number of bits to ignore at the end of the input.
        :type huffmanTree: BinTree
        :type dataIN: str
        :type alignement: int
        :return: returns the decoded text
        :rtype: str

        :Example:

        >>> decodeData(H, 'Z@¤Æ\\x07', 5)
        'bbaabtttaabtctce'
    """
    # FIXME
    s = ''
    for c in dataIN:
        val = ord(c)
        inter = ''
        while val > 0:
            inter = str(val%2) + inter  
            val = val //2
        if len(inter) < 8 and c != dataIN[len(dataIN)-1]:
            inter = '0' + inter
        s += inter  
    l = []
    encodeDataRec(huffmanTree,'',l)
    res = ''
    t = len(l)
    inter_s =''
    for i in range(len(s)):
        j = 0
        inter_s += s[i]
        b = False
        while j < t and not b:
            (elt,chemin) = l[j]
            b = chemin == inter_s
            j+=1
            
        if b:
            res += elt
            inter_s = ''
    return res

#print(decodeData(B,a,b))

def decompression(dataIN):
    """Decompress the data compressed using the Huffman algorithm :func:`compression`

        :param dataIN: the compressed data and huffman tree, and their respectives alignment bits.
        :type dataIN: tuple< tuple<str, int>, tuple<str, int> >
        :return: returns the decompressed text.
        :rtype: str

        :Example:

        >>> decompression((('Z@¤Æ\\x07', 5), ('.\\x96%\\x85c²\\x01', 7)))
        'bbaabtttaabtctce'
    """
    # FIXME
    (tuple1, tuple2) = dataIN
    (a,b) = tuple2
    L = []
    buildFrequencyList(L, 'bbaabtttaabtctce')
    B = buildHuffmanTree(L)
    (a,b) = tuple1
    return decodeData(B,a,b)

print(decompression(compression('bbaabtttaabtctce')))