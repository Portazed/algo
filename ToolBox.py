# -*- coding: utf-8 -*-
"""
ToolBox for S3: add-ons
@author: Nathalie
"""

"""
Split a list in 3: (first halh, middle, second half)
"""

def splitList(L):
    n = len(L)
    L1 = []
    for i in range(n//2):
        L1.append(L[i])
    L2 = []
    for i in range(n//2+1, n):
        L2.append(L[i])
    return (L1, L[n//2], L2)

L = [1, 2, 3, 4, 5, 6, 7, 8, 9]
splitList(L)

def splitList(L):
    n = len(L)
    return (L[:n//2], L[n//2], L[n//2+1:])

'''
Search in Lists
'''


def binarySearch(L, x, left, right):
    """
    returns the position where x is or might be
    """
    if right <= left:
        return right
    mid = left + (right-left)//2
    if L[mid] == x:
        return mid
    elif x < L[mid]:
        return binarySearch(L, x, left, mid)
    else:
        return binarySearch(L, x, mid+1, right)

def listSearch(x, L):
    i = binarySearch(L, x, 0, len(L))
    if i < len(L) and L[i]  == x:
        return i
    else:
        return -1

# introducing ternary operator

def listSearch2(x, L):
    i = binarySearch(L, x, 0, len(L))
    return i if (i < len(L)) and (L[i] == x) else -1


from random import randint

def buildList(nb, val = None, alea = None):
    #FIXME

[randint(0,10)] * 5 # does not works...


# pb
L = buildList(10, [])
L
L[0].append(2)
L

# last version
def buildList(nb, val = None, alea = None):
    #FIXME

M = ???
M
M[0][0] = 1
M

def buildMatrix(line, col, val = None):
    #FIXME
