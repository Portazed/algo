# -*- coding: utf-8 -*-
"""
Queue interface
February 2016
@author: Nathalie & Ted
"""

def newQueue():
    import queue
    return queue.Queue()

def isEmpty(q):
    return q.empty()

def enqueue(e, q):
    q.put(e)
    return q

def dequeue(q):
    if q.empty():
        raise Exception("Empty queue")
    return q.get()
