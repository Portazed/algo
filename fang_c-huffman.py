__license__ = 'GolluM (c) EPITA'
__docformat__ = 'reStructuredText'
__revision__ = '$Id: huffman.py 2016-04-04'

from binaryTrees import *

################################################################################
## COMPRESSION

def buildFrequencyList(outputList, dataIN):
    (length,j) = (len(dataIN),0)
    B=True
    for i in range(length):
        outputList.append((0,''))
    for x in range (length):
        while j < length and B :
            (val,e) = outputList[j]
            if e == '' :
                outputList[j] = (1,dataIN[x])
                B = False
            elif e == dataIN[x]:
                outputList[j] = (val+1,e)
                outputList.pop()
                B = False
            j+=1
        (B,j) =(True,0)
    return outputList

def buildHuffmanTree(inputList):
    Length = len(inputList)
    for i in range (Length-1):
        for j in range (Length-1-i):
            if inputList[j] < inputList[j+1]:
                 (inputList[j],inputList[j+1]) = (inputList[j+1],inputList[j])
    L=[]
    for i in range(Length):
        (val,e) = inputList[i]
        L.append(newBinTree(e,None,None))
    while Length != 1:
        ((val,e),(val1,e1)) = (inputList[Length-2],inputList[Length-1])
        key = val + val1
        L[Length-2] = newBinTree(newBinTree('.',None,None),L[Length-2],L[Length-1])
        inputList[Length-2] = (key,e)
        pos = Length-2
        while pos > 0 and inputList[pos] > inputList[pos-1] :
            (inputList[pos],inputList[pos-1]) = (inputList[pos-1],inputList[pos])
            (L[pos],L[pos-1]) = (L[pos-1],L[pos])
            pos -=1
        Length -=1
    return L[0]


def encodeData_rec(H,c,occ=""):
    if H.left == None:
        if H.key == c:
            return occ
        else:
            return "no code found"
    else:
        occr = encodeData_rec(H.left,c,occ+'0')
        if (occr != "no code found"):
            return occr
        return encodeData_rec(H.right,c,occ+'1')


def encodeData(huffmanTree, dataIN):
    s = ""
    for i in range (len(dataIN)):
        s +=encodeData_rec(huffmanTree,dataIN[i],"")
    return s


def todecimal(s):
    r = 0
    for c in s:
        r *= 2
        if c == '1':
            r += 1
    return r

def binaire(x):
    s=""
    i=0
    while x>0:
        s = str(x%2) +s
        x//=2
        i+=1
    while i <8:
        s = '0' + s
        i+=1
    return s

def encodeTree_rec(H,s=""):
    if H == None:
        return ""
    else:
        if H.left != None:
            return '0'+ encodeTree_rec(H.left, s) + encodeTree_rec(H.right ,s)
        else:
            return s+'1'+binaire(ord(H.key))
def encodeTree(huffmanTree):
    return encodeTree_rec(huffmanTree,"")


def toBinary(dataIN):
    string =""
    Length = len(dataIN)
    (i,x) = (0,0)
    while i+8 < Length:
        s = ""
        for j in range(i,i+8):
            s += dataIN[j]
        string += chr(todecimal(s))
        i+=8
    (Add,s) = (Length-i,"")
    for z in range(i,Length):
        s+=dataIN[z]
    for y in range(Add,8):
        x += 1
    string += chr(todecimal(s))
    return (string,x)

def compression(dataIN):
    return (toBinary(encodeData(buildHuffmanTree(buildFrequencyList([], dataIN)),dataIN)),toBinary(encodeTree(buildHuffmanTree(buildFrequencyList([], dataIN)))))




################################################################################
## DECOMPRESSION


def decode(dataIN, alignement):
    (s,Add) = ('','')
    for i in range(len(dataIN)-1):
        s += binaire(ord(dataIN[i]))
    i+=1
    Add = binaire(ord(dataIN[i]))
    for j in range(alignement,8):
        s += Add[j]
    return s



def decodeTree(dataIN, alignement):
    s = decode(dataIN, alignement)
    Length = len(s)
    (H,i) = decodeTree_rec(s,0,Length)
    return H

def decodeTree_rec(s,i,Length):
    if i < Length:
        if s[i] == '0':
            H = newBinTree('.',None,None)
            i+=1
            (H.left,i) = decodeTree_rec(s,i,Length)
            (H.right,i) = decodeTree_rec(s,i,Length)
        else:
            i+=1
            (string,j) = ('',i+8)
            while i < j:
                string += s[i]
                i+=1
            H  = newBinTree(chr(todecimal(string)),None,None)
        return (H,i)
    else:
        return (None,i)

def decodeData(huffmanTree, dataIN, alignement):
    s = decode(dataIN, alignement)
    (H,string) = (huffmanTree,'')
    (Length,i) = (len(s),0)
    while i< Length:
        if H.left == None:
            string += H.key
            H = huffmanTree
        else:
            if s[i]== '0':
                H=H.left
                i+=1
            else :
                H = H.right
                i+=1
    string += H.key
    return string


def decompression(dataIN):
    (x,y) = dataIN
    ((a,b),(c,d)) = (x,y)
    H = decodeTree(c,d)
    return decodeData(H, a,b)
