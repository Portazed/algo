from BinaryTrees import*

class AVL:
    def __init__(self,key,left,right,balance):
        self.key = key
        self.left = left
        self.right = right
        self.balance = balance

def BST2AVL_rec(B):
    if B == None:
        return (None, -1)
    (left,hLeft) = BST2AVL_rec(B.left)
    (right, hRight) = BST2AVL_rec(B.right)
    A = AVL(B.key, left, right, hLeft - hRight)
    return (A, 1 + max(hLeft,hRight))

def BST2AVL(B):
    return BST2AVL_rec(B)[0]

def isHEqui_rec(B):
    if B == None:
        return(True, -1)
    (leftHEqui, hLeft) = isHEqui_rec(B.left)
    if not leftHEqui:
        return(False, -1)
    (rightHEqui, hRight) = isHEqui_rec(B.right)
    return (rightHEqui and abs(hLeft - hRight) < 2, 1 + max(hLeft, hRight))

def isHEqui(B):
    return isHEqui_rec(B)[0]

def UpdateEqui(B):
    if B == None:
        return -1
    hLeft = UpdateEqui(B.left)
    hRight = UpdateEqui(B.right)
    B.balance = hLeft - hRight
    return 1 + max(hLeft, hRight)

def RotateLeft(B):
    C = B.right;
    B.right = C.left
    C.left = B
    B = C
    B.left.balance = - 1 - B.balance
    B.balance = 1 + B.balance
    return B

def RotateRight(B):
    C = B.left
    B.left = C.right
    C.right = B
    B = C
    B.right.balance = 1 - B.balance
    B.balance = - 1 + B.balance
    return B

def RotateLeftRight(A):
    aux = A.left.right
    A.left.right = aux.left
    aux.left = A.left
    A.left = aux.right
    aux.right = A
    A = aux
    if A.balance == -1:
        (A.left.balance, A.right.balance) = (0,1)
    elif A.balance == 1:
        (A.left.balance, A.right.balance) = (-1,0)
    else:
        (A.left.balance, A.right.balance) = (0,0)
    A.balance = 0
    return A

def RotateRightLeft(A):
    aux = A.right.left
    A.right.left = aux.right
    aux.right = A.right
    A.right = aux.left
    aux.left = A
    A = aux
    if A.balance == -1:
        (A.left.balance, A.right.balance) = (1,0)
    elif A.balance == 1:
        (A.left.balance, A.right.balance) = (0,-1)
    else:
        (A.left.balance, A.right.balance) = (0,0)
    A.balance = 0
    return A

def printAVL(B, s=""):
    '''
    display from GolluM/Nath
    '''
    if B == None:
        print(s, '- ')
    elif B.left == None and B.right == None:
        print(s, '- ', B.key, ', ', B.balance)
    else:
        print(s, '- ', B.key, ', ', B.balance)
        printAVL(B.left, s + "  |")
        printAVL(B.right, s + "   ")

def addAVL(x, B):
    if B == None:
        return AVL(x, None, None,0)
    else:
        if x == B.key:
            return B
        if x < B.key:
            B.left = addAVL(x, B.left)
            B.balance = 1 + B.balance
        else:
            B.right = addAVL(x, B.right)
            B.balance = -1 + B.balance
        if B.balance == 2:
            if B.left.balance == 1:
                B = RotateRight(B)
            if B.left.balance == -1:
                B = RotateLeftRight(B)
        if B.balance == -2:
            if B.right.balance == -1:
                B = RotateLeft(B)
            if B.right.balance == 1:
                B = RotateRightLeft(B)
        return B

"""
B = AVL('F', AVL('B', AVL('A', None,None,0), AVL('D', AVL('C',None,None,0),AVL('E', None, None,0),0), 0), AVL('G', None, None, 0),0)
UpdateEqui(B)
printAVL(B)
B = RotateLeftRight(B)
printAVL(B)
"""
A = AVL(10, AVL(5,AVL(2,None,None,0),AVL(7, None,None,0),0), AVL(12,None,None,0), 1)
#UpdateEqui(A)
printAVL(A)
A = addAVL(1,A)
#UpdateEqui(A)
printAVL(A)
