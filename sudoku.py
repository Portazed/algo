grille = [[6,1,5,0,0,2,0,0,0],
          [4,7,0,0,0,3,0,5,0],
          [0,0,3,1,0,0,4,0,0],
          [9,0,7,0,8,0,2,0,0],
          [3,0,0,0,4,0,0,0,9],
          [0,0,8,0,2,0,5,0,7],
          [0,0,9,0,0,4,1,0,0],
          [0,6,0,7,0,0,0,2,4],
          [0,0,0,9,0,0,7,3,5]]

grille2 = [[5, 3, 0, 0, 7, 0, 0, 0, 0],
        [6, 0, 0, 1, 9, 5, 0, 0, 0],
        [0, 9, 8, 0, 0, 0, 0, 6, 0],
        [8, 0, 0, 0, 6, 0, 0, 0, 3],
        [4, 0, 0, 8, 5, 3, 0, 0, 1],
        [7, 0, 0, 0, 2, 0, 0, 0, 6],
        [0, 6, 0, 0, 0, 7, 2, 8, 4],
        [0, 0, 0, 4, 1, 9, 0, 3, 5],
        [0, 0, 0, 0, 8, 0, 0, 7, 9]]

#Ce que j'ai fait
def print_grid(G):
    print(' ------- ------- ------- ')
    for i in range(9):
        print('|', end=' ')
        for j in range(9):
            if G[i][j] == 0:
                print(' ', end=' ')
            else:
                print(G[i][j], end=' ')
            if (j + 1) % 3 == 0:
                print('|', end=' ')
        print()
        if (i + 1) % 3 == 0:
            print(' ------- ------- ------- ')

def line(g,r):
    return g[r]

def column(g,c):
    h = len(g)
    l = []
    for i in range(h):
        l.append(g[i][c])
    return l

def square(M,x,y):
    Sq = []

    x = (x//3)*3
    y = (y//3)*3

    for i in range(x, x+3):

        for j in range(y, y+3):
            Sq.append(M[i][j])
    return Sq

def is_filled(l):
    i = 0
    t = len(l)

    while i<t and l[i] != 0:
        i += 1
    return i == t

def is_correct(l):
    b = True
    t = len(l)
    for i in range(1,10):

        for j in range(t):
            b = b and i != l[j]
    return b

def is_completed(g):
    b = True

    for i in range(9):

        b = b and is_filled(get_row(g,i)) and is_filled(get_column(g,i)) and is_filled(square(g,i,i))

    return b

def is_finished(g):
    b = True

    for i in range(9):

        b = b and is_correct(get_row(g,i)) and is_correct(get_column(g,i)) and is_correct(square(g,i,i))

    return b

def find_val2(g, i, j):
    l = [True, True, True, True, True, True, True, True, True, True]
    res = []
    row = line(g, i)
    col = column(g, j)
    sq = square(g, i, j)
    for i in range(9):
        l[row[i]] = False
        l[col[i]] = False
        l[sq[i]] = False
    for i in range(1, 10):
        if l[i]:
            res.append(i)
    return res


#Correction
def str2int(L):
    for i in range(len(L)):
        L[i] = int(L[i])
    return L

def count_blank(S):
    res = 0
    for i in range(9):
        for j in range(9):
            if S[i][j] == 0:
                res += 1
    return res

def new_liste(fin, debut):
    R = []
    for i in range(debut, fin):
        R.append(0)
    return R

def values(L):
    H = new_liste(10, 0)
    for i in range(len(L)):
        H[L[i]] += 1
    return H

def first_empty(H):
    i = 1
    while i <= 9 and H[i] != 0:
        i += 1
    return i

def find_val(G, x, y):
    Hline = values(line(G, x))
    #if Hline[0] == 1:
    #    return [first_empty(Hline)]

    Hcol = values(column(G, y))
    #if Hcol[0] == 1:
    #    return [first_empty(Hcol)]

    Hsquare = values(square(G, x, y))
    #if Hsquare[0] == 1:
    #    return [first_empty(Hsquare)]

    Lval = []
    for i in range(1, 10):
        if Hline[i] == 0 and Hcol[i] == 0 and Hsquare[i] == 0:
            Lval.append(i)
    return Lval

def replace(G):
    for i in range(9):
        for j in range(9):
            if G[i][j] == 0:
                val = find_val(G, i, j)
                if len(val) == 1:
                    G[i][j] = val[0]
                    return True #FIXME
    return False

def sudoku1(G):
    n = count_blank(G)
    while n != 0 and replace(G):
        n -= 1
    return n == 0

def rmf(l):
    for i in range(0,len(l)-1):
        l[i] = l[i+1]
    return l

def BackTrack(G):
    if count_blank(G) == 0:
        return True
    """i = pos//9
    j = pos//9

    if(G[i][j] != 0):
        return BackTrack(G, pos+1)

    val = find_val(G, i, j)

    G[i][j] = val[0]
    pos += 1

    if len(val) == 0:
        return False"""
    for i in range(9):
        for j in range(9):
            if G[i][j] == 0:
                val = find_val(G, i, j)
                if len(val) >= 1:
                    G[i][j] = val[0]
                    val = rmf(val)
                    if(BackTrack(G)):
                        return True

    return False




#grille2 = load_grid('SudoG.txt')

print_grid(grille2)
#print(sudoku1(grille))

BackTrack(grille2)
print_grid(grille2)

#print_grid(Sudo)
#res_sudo(Sudo)
#print_grid(Sudo)

input('Appuyez sur une touche pour continuer... ')
