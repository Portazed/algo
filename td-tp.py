# -*- coding: utf-8 -*-
"""
Created on Wed May 11 23:12:07 2016

@author: epita-student
"""
from BinaryTrees import *

Z = newBinTree(3,None,None)
C = newBinTree(0,None,None)
D = newBinTree(2,None,Z)
B = newBinTree(1,C,D) 

#printTree(B)


def listtoABR(l,i,t):
    if i == 0 or i == t-1:
        B = BinTree()
        B.key = l[i]
        B.left = None
        B.right = None
        return B        
    elif i< t:
        B = BinTree()
        B.key = l[i]
        B.left = listtoABR(l, i//2,t)
        B.right = listtoABR(l, t-1-i//2,t)
        return B
    else:
        return None

h = [1,2,3,4,5,6,7]
t = len(h)
#A = listtoABR(h,t//2,t)
#printTree(A)

def ABRtolist(B,l=[]):
    if B != None:
        ABRtolist(B.left,l)
        l.append(B.key)
        ABRtolist(B.right,l)
    
#l = []
#ABRtolist(B,l)
#print(l)
    
def to_graphiz_rec(B,s=""):
    if B != None:        
        if B.left!= None:  
            s+="\n  "
            s+= str(B.key)
            s+=" -- "
            G = B.left
            s+= str(G.key)
            s = to_graphiz_rec(B.left, s)
            
        if B.right!= None:  
            s+="\n  "
            s+= str(B.key)
            s+=" -- "
            D = B.right
            s+= str(D.key)
            s = to_graphiz_rec(B.right, s)
    return s

def to_graphiz(B):
    print("graph Arbre {\n")
    s = ""
    s = to_graphiz_rec(B,s)
    print(s)
    print("\n}")
   
print(to_graphiz(B))