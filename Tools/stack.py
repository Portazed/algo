from collections import deque

class Stack():
    
    def __init__(self):

        self.elements = deque()

def push(S,e):
    S.elements.appendleft(e)

def pop(S):
    return S.elements.popleft()

def isEmpty(S):
    return 0 == S.elements.count()
