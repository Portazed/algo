# -*- coding: utf-8 -*-
"""
Created on Sept. 2016

@author: nb, gd
"""

# General Tree class
# ------------------------------------------------------------------------------

class Tree:
    """
    Simple class for General Tree
    """

    def __init__(self, key=None, children=None):
        """
        Init General Tree, ensure children are properly set.
        """
        self.key = key
        if children is not None:
            self.children = children
        else:
            self.children = []

    @property
    def nbChildren(self):
        return len(self.children)
        
        
def tutoEx1():
    C1 = Tree(3, [Tree(-6), Tree(10)])
    C2 = Tree(8, [Tree(11, [Tree(0), Tree(4)]), Tree(2), Tree(5)])
    C3 = Tree(9)
    return Tree(15, [C1, C2, C3])
    

# measures
#------------------------------------------------------------------------------

def size(T):
    if T.nbChildren == 0:
        return 1
    t = 0
    for i in range (T.nbChildren):
        t += size(T.children[i])

    return 1 + t

# height

def height(T):
    if T.nbChildren == 0:
        return 0

    maxH = 0
    for i in range(T.nbChildren):
        maxH = max(maxH,height(T.children[i]))

    return 1 + maxH
    
# External Path Length
        
def epl(T, h=0):
    if T.nbChildren == 0:
        return h

    height = 0
    for i in range(T.nbChildren):
        height += epl(T.children[i], h+1)

    return height

