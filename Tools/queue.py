from collections import deque

class Queue():
    
    def __init__(self):
        
        self.elements = deque()

def enqueue(Q,e):
    Q.elements.appendleft(e)

def dequeue(Q):
    return Q.elements.pop()

def isEmpty(Q):
    return 0 == Q.elements.count()
