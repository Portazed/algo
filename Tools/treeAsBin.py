# -*- coding: utf-8 -*-
"""
Created on Sept. 2016

@author: nb, gd
"""

class TreeAsBin:
    """
    Simple class for (General) Trees 
    represented as Binary Trees (first child - right sibling)
    """

    def __init__(self, key, child=None, sibling=None):
        """
        Init Tree
        """
        self.key = key
        self.child = child
        self.sibling = sibling


def tutoEx1():
    C1 = TreeAsBin(3, TreeAsBin(-6, None, TreeAsBin(10)))
    C2 = TreeAsBin(8, TreeAsBin(11, TreeAsBin(0, None, TreeAsBin(4)), 
                                TreeAsBin(2, None, TreeAsBin(5))))
    C3 = TreeAsBin(9)
    C1.sibling = C2
    C2.sibling = C3
    return TreeAsBin(15, C1, None)
    
# measures
#------------------------------------------------------------------------------

def size(B):
    if B == None:
        return 0

    return 1 + size(B.child) + size(B.sibling)
    
def size2(B):
    #FIXME

# height
    
def height(B):
    if B == None:
        return -1

    return max(1 + height(B.child), height(B.sibling))

def height2(B):
    #FIXME

# External Path Length


def epl(B):
    if B == None:
        return h-1

    return epl(B.child, h+1) + epl(B.sibling, h)
