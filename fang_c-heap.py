"""
Heap homework
@author: login
"""

def newHeap():
    return [None]
    
def isEmpty(H):
    return len(H) == 1

H = [None,(2,'A'),(12,'B'),(10,'C'),(24,'D'),(16,'E'),(14,'F'),(18,'G'),(30,'H'),(26,'I'),(20,'J'),(32,'K'),(28,'L'),(22,'M')]
He= [None, (1, 'P'), (2, 'A'), (5, 'N'), (12, 'B'), (16, 'E'), (14, 'F'), (10, 'C'), (24, 'D'), (26, 'I'), (20, 'J'), (32, 'K'), (28, 'L'), (22, 'M'), (18, 'G'), (15, 'O'), (30, 'H')]

def heapPush(H,x):    
    i=len(H)                     
    H.append(x)
    while i > 1 and x < H[i//2]: 
        (H[i],H[i//2]) = (H[i//2] , H[i])        
        i//=2          
    return H
        

def heapPop(H):
    if isEmpty(H):
        return None
    (B,length)= (True,len(H))     
    i=1      
    (H[1],H[length-1]) = (H[length-1],H[1])
    Mini = H.pop()
    length-=1 
    while B and 2*i < length :     
        if 2*i+1 == length :
            if H[2*i] < H[i]:
                i*=2
                swap=H[i]
            else :
                B=False
        else:
            if H[2*i]<H[2*i+1]:
                i*=2
                swap=H[i]
            else:
                i=i*2+1
                swap=H[i]          
        if B and H[i//2] > swap :
            (H[i],H[i//2]) = (H[i//2],H[i])
        elif B and H[i//2] <= swap:
            B=False
    return Mini
    
def heapUpdate(H, x, pos):
    length = len(H)
    if pos <= 0 or pos >= length :
        raise Exception('mauvaise valeur pos')    
    H[pos] = x
    while pos > 1 and x < H[pos//2]: 
        (H[pos],H[pos//2]) = (H[pos//2] , H[pos])        
        pos//=2          
    return H
    
    
    
    

#------------------------------------------------------------------------------


def heapSort(L):
    length = len(L)
    H = newHeap()
    for i in range(length):
        heapPush(H,L[i])
    for i in range(length):
        L[i]=heapPop(H)
    return L

    
    
def heapify(H):
    for i in range (1,len(H)):
        heapUpdate(H,H[i],i)
    return H 
