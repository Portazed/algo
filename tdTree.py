from AlgoQueue import*

def DFT(B): #Depth-first Traversal
    if B == None:
        return True
    else:
        #Prefix Treatment
        DFT(B.left)
        #Infix Treatment
        DFT(B.right)
        #Suffix Treatment

def BFT(B): #Breadth-first Traversal with level change
    if b != None:
        q = newQueue()
        q = enqueue(B,q)
        q = enqueue(None,q)

        while not isEmpty(q):
            B = dequeue(q)

            if B == None:
                #Treatment

                if not isEmpty(q):
                    q = enqueue(None,q)
            else:
                if B.left != None:
                    q = enqueue(B.left, q)

                if B.right != None:
                    q = enqueue(B.right, q)

def degenerate(B):
    if B == None:
        return True
    elif B.left == None:
        return degenerate(B.right)
    elif B.right == None:
        return degenerate(B.left)
    else:
        return False

def perfect(B):
    d = True
    c = 1
    n = 1
    q = newQueue()
    q = enqueue(B, q)
    q = enqueue(None, q)

    while not isEmpty(q):
        B = dequeue(q)

        if B == None:
            d = d and n == c
            n *= 2
            c = 0
            if not isEmpty(q):
                q = enqueue(None, q)
        else:
            c += 1

            if B.left != None:
                q = enqueue(B.left, q)

            if B.right != None:
                q = enqueue(B.right, q)
    return d

def complete(B):
