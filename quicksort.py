def quick_sort(l,bs,bi):
    n = bs - bi + 1
    if n>=2:
        p=bi
        vi = bi + 1
        for i in range(bi+1,bs+1):
            if l[i]<l[p]:
                (l[i],l[vi]) = (l[vi], l[i])
                vi += 1
        (l[p],l[vi-1]) = (l[vi-1],l[p])
        quick_sort(l,bi,vi-2)
        quick_sort(l,vi,bs)
