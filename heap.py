from binaryTrees import *


def newHeap():
    return [None]

def isEmpty(H):
    return len(H) == 1

def HeapPush(H,x, i=1):
    if isEmpty(H):
        H.append(x)
    else:
        H.append(None)
        a = H[1]
        H[1] = x
        for i in range(2,len(H)):
           (H[i],a) = (a,H[i])

        i = 1
        while i <= (len(H) // 2) - 1 and (H[i] > H[i * 2] or H[i] > H[i * 2 + 1]):

            if i * 2 == len(H) or H[i * 2 + 1] > H[i * 2]:
                j = i * 2
            else:
                j = i * 2 + 1
            var = H[i]
            H[i] = H[j]
            H[j] = var
            i = j

def HeapPop(H):

    if not isEmpty(H):
        res = H[1]
        H[1] = H[len(H) - 1]
        H.pop()
        i = 1
        while i <= (len(H) // 2) - 1 and (H[i] > H[i * 2] or H[i] > H[i * 2 + 1]):

            if i * 2 == len(H) or H[i * 2 + 1] > H[i * 2]:
                j = i * 2
            else:
                j = i * 2 + 1
            var = H[i]
            H[i] = H[j]
            H[j] = var
            i = j
        return res
    else:
        return H

def HeapUpdate(H, x, c):
    if isEmpty(H):
        return H
    else :
        ch = ''
        i = 1
        while i < len(H) and c != ch:
            (entier,ch) = H[i]
            i += 1

        i -= 1
        while i < len(H) - 1:
            H[i] = H[i+1]
            i+=1
        H.pop()
        HeapPush(H,x)


def HeapSort(L):
    H = newHeap()
    for i in range(len(L)):
        HeapPush(H,L[i])
    l1 = []
    while not isEmpty(H):
        l1.append(HeapPop(H))
    return l1

def Heapify(L):
    H = newHeap()
    t = len(L)
    if(t > 1):
        for i in range(1, t):
            HeapPush(H,L[i])
    return H

"""
H = newHeap()
print(H)
HeapPush(H, (5, "O"))
print(H)
HeapPush(H, (7, "N"))
print(H)
HeapPush(H, (1, "A"))
print(H)
HeapPush(H, (2, "K"))
print(H)
HeapPush(H, (8, "P"))
print(H)
HeapPop(H)
print(H)
[None,(20,'A'),(5,'B'),(10,'C'),(12,'D'),(15,'E'),
            (8,'F'),(2,'G'),(6,'H'),(2,'I'),(9,'J')]

G = Heapify([None,(20,'A'),(5,'B'),(10,'C'),(12,'D'),(15,'E'),
           (8,'F'),(2,'G'),(6,'H'),(2,'I'),(9,'J')])
print(G)
"""
