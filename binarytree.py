# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 08:43:52 2016
@author: Nathalie
"""

class BinTree:
    '''
    define the name
    '''

# Binary Tree factory helper function
def newBinTree(key, left, right):
    B = BinTree()
    B.key = key
    B.left = left
    B.right = right
    return B

# Example Binary Trees
bEx1 = newBinTree(1,
                  newBinTree(2,
                             newBinTree(4, newBinTree(8, None, None), None),
                             newBinTree(5, None, newBinTree(11, None, None))),
                  newBinTree(3,
                             newBinTree(6, None, None),
                             newBinTree(7,
                                        newBinTree(14, None, newBinTree(29, None, None)),
                                        None)))


B_tuto = newBinTree('V',
            newBinTree('D',
               newBinTree('I',
                  newBinTree('Q', None,newBinTree('U', None,None)),
                  None),
               newBinTree('S',
                  newBinTree('E', None,None),
                  newBinTree('T', None,None))),
            newBinTree('I',
               newBinTree('E',
                  None,
                  newBinTree('R', None,None)),
               newBinTree('I',
                  newBinTree('A', newBinTree('T', None,None),newBinTree('S', None,None)),
                  None)))

L_tutoPref = ['V', 'D', 'I', 'Q', 'U', 'S', 'E', 'T', 'I', 'E', 'R', 'I',
            'A', 'T', 'S']

L_tuto = ['V', 'D', 'I', 'Q', '#', 'U', '#', '#', '#', 'S', 'E', '#', '#',
          'T', '#', '#', 'I', 'E', '#', 'R', '#', '#', 'I', 'A', 'T', '#',
          '#', 'S', '#', '#']

def size(B):
    if B == None:
        return 0
    else:
        return 1 + size(B.left) + size(B.right)

def height(B):
    if B == None:
        return -1
    else:
        return 1 + max(height(B.left), height(B.right))
