# -*- coding: utf-8 -*-
"""
Files and frequencies
Sept. 2016
@author: Nathalie
"""


""" use this function to build files """

import random

def buildExFileForAssoc(fileName, n):
    """
    build a file with n lines of 80 characters (lower letters)
    """
    f = open(fileName, 'w')
    for i in range(n):
        s = ""
        for j in range(1, 80):
            s += chr(97 + random.randint(0,2) * random.randint(0, 12))
        f.write(s + '\n')
    f.close()

# buildExFileForAssoc("test.txt", 20)

""" Examples of reading files """

def loadFileToList(fileName):
    f = open(fileName)
#    first = f.readline() #reads the current (first here) line
    lines = f.readlines() # reads all the remaining lines  (same as list = lines(f))
    f.close()
    return lines


#L = loadFileToList("test.txt")
# -> result: each line is an element of the list (a string), with the \n at the end

def loadFileToList2(file):
    f = open(file)
    L = []
    for line in f:
        L.append(line.strip())  # .strip() remove the '\n'
    f.close()
    return L

#L2 = loadFileToList2("test.txt")


"""
Associative lists
"""

L = [('w', 25), ('j', 38), ('q', 31), ('b', 40), ('g', 59), ('i', 51),
     ('a', 454), ('c', 63), ('k', 68), ('m', 60), ('u', 27), ('s', 29),
     ('f', 25), ('y', 31), ('d', 42), ('e', 55), ('h', 30), ('o', 39),
     ('l', 33)]

"""
exercises: findChar, assoc and buildAssocList (?)
"""

def findChar(c, L): # O(n)
    """
    if found, returns the position of the first pair (c, ?) in L
    returns -1 otherwise
    """
    i = 0
    n = len(L)

    char_ = ''

    if L:
        char_ = L[0]
    else:
        return -1

    while i < n and c != char_:
        char_ = L[i][0]
        print(L[i][0])
        i+=1

    return i-1 if i < n else -1

print(findChar('j', L))

def assoc(c, L):
    """
    L is a list of pairs (key, value)
    returns the value associated with c in L or None if not found
    """
    pos = findChar(c,L)

    return L[pos][1] if pos > -1 else None

print(assoc('j', L))

def buildAssocList(fileName):
    """
    build an associative list from the file fileName
    the list contains pairs (character, nb of occurrences)
    """
    L = []
    text = loadFileToList2(fileName)
    n = len(text)

    for i in range(n):
        chaine = text[i]
        for c in chaine:
            pos = findChar(c, L)
            if pos != -1:
                occ = L[pos][1]
                L[pos] = (c ,occ + 1)
            else:
                L.append((c,1))

    return L

#buildExFileForAssoc("/home/pleuvens/Documents/GitRepo/Algo/algo/testTD1", 5)

L2 = buildAssocList("/home/pleuvens/Documents/GitRepo/Algo/algo/testTD1")
print(L2)
