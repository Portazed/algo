from BinaryTrees import*
from avl import*

def insert_leaf(B,x):
    if B == None :
        B = newBinTree(x,None,None)
    else:
        if x == B.key:
            return False
        if x < B.key:
            return insert_leaf(B.left,x)
        if x > B.key:
            return insert_leaf(B.right,x)
    return True

def test_deseq(A):
    if A == None:
        return True;
    else:
        return (A.balance < 2 && A.balance > -2) && test_deseq(A.left) && test_deseq(A.right)

def Equals(A,B,length,i=1):
    if(i >= length):
        return B == None
    else:
        if(B == None):
            return l[i] == None
        else:
            isTrue = B.key == A[i]
            if(isTrue):
                return  Equals(A,B.left,2*i) && Equals(A,B.right,2*i+1)
            else:
                return False;
