# -*- coding: utf-8 -*-
"""
Created on Sept. 2016

@author: nb, gd
"""

# General Tree class
# ------------------------------------------------------------------------------

class Tree:
    """
    Simple class for General Tree
    """

    def __init__(self, key=None, children=None):
        """
        Init General Tree, ensure children are properly set.
        """
        self.key = key
        if children is not None:
            self.children = children
        else:
            self.children = []

    @property
    def nbChildren(self):
        return len(self.children)

    

# measures
#------------------------------------------------------------------------------

def size(T):
    s = 1
    for i in range(T.nbChildren):
        s += size(T.children[i])
    return s


# height

def height(T):
    h = -1
    for i in range(T.nbChildren):
        h = max(h, height(T.children[i]))
    return h + 1
    
# External Path Length
        
def epl(T, h=0):
    """
    External Path Length
    """
    if T.nbChildren > 0:
        length = 0
        for i in range(T.nbChildren):
            length += epl(T.children[i], h+1)
        return length
    else:
        return h

def print_tree(T, i = 0):
    if T:
        espace = '  '
        print("- " + espace * i + T.key[0])

        for c in T.children:
            print_tree(c,i+1)
