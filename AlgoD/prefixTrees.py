__license__ = 'Nathalie (c) EPITA'
__docformat__ = 'reStructuredText'
__revision__ = '$Id: lexicalTrees.py 2016-10-10'


from AlgoPy import tree
from AlgoPy.tree import Tree

################################################################################
## EXAMPLE

Tree1 = Tree(('', False),
             [Tree(('c', False),
                   [Tree(('a', False),
                         [Tree(('s', False),
                               [Tree(('e', True)),
                                Tree(('t', True),
                                     [Tree(('l', False),
                                           [Tree(('e', True))])])])]),
                    Tree(('i', False),
                         [Tree(('r', False),
                               [Tree(('c', False),
                                     [Tree(('l', False),
                                           [Tree(('e', True))])])]),
                          Tree(('t', False),
                               [Tree(('y', True))])]),
                    Tree(('o', False),
                         [Tree(('m', False),
                               [Tree(('e', True))]),
                          Tree(('u', False),
                               [Tree(('l', False),
                                     [Tree(('d', True))])])])]),
             Tree(('f', False),
                [Tree(('a', False),
                       [Tree(('m', False),
                             [Tree(('e', True)),
                              Tree(('o', False),
                                   [Tree(('u', False),
                                         [Tree(('s', True))])])]),
                         Tree(('n', True),
                             [Tree(('c', False),
                                   [Tree(('y', True))])])])])])
                      
################################################################################
## MEASURES
                        
def countWords(T):
    """ count words in dictionnary T
    
    :param T: The prefix tree
    :rtype: int
    :Example:    
    >>> countWords(Tree1)
    11
    """
    
    if T:
      nbwords = T.key[1]

      for c in T.children:
        nbwords += countWords(c)

      return nbwords

def longestWordLength(T):
    """ longest word length
    
    :param T: The prefix tree
    :rtype: int
    :Example:
    >>> longestWordLength(Tree1)
    6
    """
    if T:
      return tree.height(T)
      

def _averageLengthRec(T, h = 0):
    if T:
      taille = T.key[1] * h
      nbWords = T.key[1]
      for c in T.children:
        (t,nb) = _averageLengthRec(c, h+1)
        taille += t
        nbWords += nb
        
      return (taille, nbWords)
 
def averageLength(T):
    """ average word length
    :param T: The prefix tree
    :rtype: float
    :Example:
    >>> averageLength(Tree1)
    4.636363636363637
    """
    L = [0]
    (taille, nbW) = _averageLengthRec(T)
    return taille / (nbW)

    
###############################################################################
## Researches


def searchWord(T, word, i = 0):
    """ search for a word in dictionary
    
    :param T: The prefix tree
    :param word: The word to search for
    :rtype: bool
    :examples:
    >>> searchWord(Tree1, "fabulous")
    False
    >>> searchWord(Tree1, "Famous")
    True
    """
    if T:   

        isHere = True
        count = 0
        if(i == len(word) and not T.key[1]):
            return False
        for c in T.children:
            if c.key[0] == word[i].lower():
                isHere = isHere and searchWord(c, word, i+1)
                count += 1
        
        return isHere and (count > 0 or i == len(word))

###############################################################################
## Lists

    
def wordList(T, s = ""):
    """ generate the word list
    
    :param T: The prefix tree
    :rtype: list
    :example:
    >>> print(wordList(Tree1))
    ['case', 'cast', 'castle', 'circle', 'city', 'come', 'could', 'fame', 'famous', 'fan', 'fancy']
    """
    if T:
        L = []
        s += T.key[0]
        if T.key[1]:
            L.append(s)
        for c in T.children:
            L += wordList(c, s)

        return L


def completion(T, prefix, s="", i=0):
    """ generate the list of words with a common prefix
    
    :param T: The prefix tree
    :param pref: the prefix
    :rtype: list
    :examples: # result elements can have different case...
    >>> completion(Tree1, "Fan")
    ['Fan', 'Fancy']
    >>> completion(Tree1, "CI")
    ['Circle', 'City']   
    >>> completion(Tree1, "what")
    []
    """
    
    if T:
        L = []
        s += T.key[0]
        if T.key[1]:
            L.append(s)
        for c in T.children:
            if(i == len(prefix)):
                L += completion(c,prefix,s,i)
            else:
                if(c.key[0] == prefix[i].lower()):
                    L += completion(c,prefix,s,i+1)
        return L


def treeToFile(T, filename):
    """ save the dictionary in a file
    
    :param T: The prefix tree
    :param filename: the file name
    :example:
    >>> treeToFile(Tree1, "test.txt")
    # give the same file as "textFiles/wordList1.txt" but in alphabetic order
    """
    if T:
        L = wordList(T)
        print(L)
        f = open(filename,'w')
        for i in range(len(L)):
            for j in range(len(L)):
                index = 0
                while(index < min(len(L[i]),len(L[j])) and ord(L[i][index]) == ord(L[j][index])):
                    index += 1
                if(i < j):
                    if(index < min(len(L[i]), len(L[j])) and  ord(L[i][index]) > ord(L[j][index])):
                        temp = L[i]
                        L[i] = L[j]
                        L[j] = temp
                else:
                    if(index < min(len(L[i]), len(L[j])) and  ord(L[i][index]) <= ord(L[j][index])):
                        temp = L[i]
                        L[i] = L[j]
                        L[j] = temp

        for word in L:
            f.write(word + '\n')

        print(L)
        f.close()
    
###############################################################################
## thanks to GolluM


def _greatestPrefixRec(T, s = "", pref = 0): 
    if T:
        
        i = 0
        
        if pref <= 1:
            s += T.key[0]

        pref += T.key[1]
        
        if T.key[1] and T.nbChildren > 0:
            i = 1

        for c in T.children:
            (s_r,i_r,pref_r) = _greatestPrefixRec(c, s, pref)
            if(i_r > i):
                s = s_r
            i += i_r
            pref += pref_r

        return (s,i,pref)

def greatestPrefix(T):
    """
    search for the word that is prefix of most words
    :param T: the prefix tree
    :rtype: str
    :example:
    >>> greatestPrefix(Tree1)
    'cast'   # 'fan' can be an answer (they both are prefix of two words)
    """
    
    return _greatestPrefixRec(T)[0]

###############################################################################
## Build Tree

    
def addWord(T, word, i = 0):
    """ add a word in dictionary
    
    :param T: The prefix tree
    :param word: The word to add    
    """
    
    if T and i < len(word):
        
        count = 0

        for c in T.children:
            if c.key[0] == word[i].lower():
                addWord(c,word,i+1)
                count += 1

        if count == 0:
            C = Tree((word[i].lower(),i == len(word)- 1))
            T.children.append(C)
            addWord(C,word, i+1)
        

def treeFromFile(fileName):
    """ build the prefix tree from a file of words
    
    :param filename: The file name
    :rtype: Tree
    """
    
    f = open(fileName, 'r')
    lines = f.readlines()
    f.close()
    print(lines) 
    T = Tree(('',False))

    for line in lines:
        addWord(T,line.strip())
    
    return T

